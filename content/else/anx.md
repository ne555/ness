---
title: "Anx"
date: 2021-06-05
draft: false
---

A few things I use to help with my anxiety/claustrophobia.

## SSRIs:
50mg Sertraline every day for a couple of months now, I think it helps but I suspect it's making me depressed.

Edit: it fucking does

## Breathing technique:
This is a tool for __immediately__ reducing stress I learned from [the Huberman Lab Podcast](https://www.youtube.com/watch?v=ntfcfJ28eiU) I often find myself using it when on public transport  and such. {{< toggle_ title="here's a short video demonstration"  >}} {{< youtube rBdhqBGqiMc 83 >}} {{< /toggle_ >}}

## Cold showers:
I almost always take cold showers before leaving the house, I feel like this helps me and I can speculate on a few reasons why. 

## Exercise:
Jogging always helped, I wanna make it a daily habit to go for a jog or a walk for at least 20 minutes a day.

## Meditation:
I want to start doing this daily
I tried [Yoga Nidra Guided Meditation](https://www.youtube.com/watch?v=iEw5BkK9K9A "Yoga Nidra For Manifesting Your Intentions - Guided Meditation WIth Alpha/Theta/Delta Brainwaves").
I'm considering mindfulness and zazen meditation, currently reading more about them, in the meantime I wanna stick to Yoga Nidra.

## CBT

## Art
Mostly Drawing and painting, Art has saved my life countless times.

## Else
Neuroinflammation/inflamation in the brain causes anxiety?
https://en.wikipedia.org/wiki/Neuroinflammation
 Exercise is a promising mechanism of prevention and treatment for various diseases characterized by neuroinflammation.[20] Aerobic exercise is used widely to reduce inflammation in the periphery. Exercise has been shown to decrease proliferation of microglia in the brain, decrease hippocampal expression of immune-related genes, and reduce expression of inflammatory cytokines such as TNF-α. 

## Unrelated
Downregulation <- Benzodiazepines