---
title: "Art"
date: 2021-05-25T01:55:00+01:00
draft: false
---

## how to
```
cd Desktop/Main/Site
hugo new posts/my-first-post.md
```
## test
In this sequence, [Kbog](https://forum.effectivealtruism.org/users/kbog) when using supermemo each position can be a frequent blunder, a tactical exercise, a checkmate... the prompts don't give that away and I think that's better and helps with recognizing patterns in real settings and not just in tactics training for example: 
- apparently lists break the site
- well it's fixed now
- great
## main
[I make an argument here](https://forum.effectivealtruism.org/posts/g5BNwhsw2pDNLXFo2/why-accelerating-economic-growth-and-innovation-is-not) that marginal long run growth is dramatically less important than marginal x-risk. I'm not fully confident in it. But the crux could be what I highlight - whether society is on an endless track of exponential growth, or on the cusp of a fantastical but fundamentally limited successor stage. Put more precisely, the crux of the importance of x-risk is how good the future will be, whereas the crux of the importance of progress is whether differential growth today will mean much for the far future.

I would still _ceteris paribus_ pick more growth rather than less, and from what I've seen of Progress Studies researchers, I trust them to know how to do that well.

It's important to compare with long-term political and social change too. Arguably a higher priority than either effort, but also something that can be indirectly served by economic progress. One thing the progress studies discourse has persuaded me of is that there is some social and political malaise that arises when society stops growing. Healthy politics may require fast nonstop growth (though that is a worrying thing if true).

Reply

[Help me find the crux between EA/XR and Progress Studies](https://forum.effectivealtruism.org/posts/hkKJF5qkJABRhGEgF/?commentId=m8j9PpSuGakPEWZDr)

## another one